﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary2;

namespace ConsoleApp5
{
	class Program
	{
		static void Main(string[] args)
		{
			Student[] arrStudents;
			Console.Write("Введiть кiлькiсть студетiв: ");
			int cntStudents = int.Parse(Console.ReadLine());
			arrStudents = new Student[cntStudents];
			for (int i = 0; i < cntStudents; i++)
			{
				Console.Write("Введiть iм'я: ");
				string sName = Console.ReadLine();

				Console.Write("Введiть прiзвище:");
				string sSurname = Console.ReadLine();

				Console.Write("Введiть назву унiверситету:");
				string sUniversity = Console.ReadLine();

				Console.Write("Введiть витрати на проїзд за день :");
				string sStudentCard = Console.ReadLine();

				Console.Write("Введiть кiлькiсть навчальних днiв:");
				string sCourse = Console.ReadLine();

				Console.Write("Чи є у вас стiпендiя? (y-так, n-нi): ");
				ConsoleKeyInfo keyHasStipend = Console.ReadKey();

				Console.WriteLine();
				Console.Write("Чи навчаєтесь ви на бюджетi? (y-так, n-нi): ");
				ConsoleKeyInfo keyHasBudget = Console.ReadKey();
				Console.WriteLine();


				Console.WriteLine();
				Student OurStudent = new Student();

				OurStudent.Name = sName;
				OurStudent.Surname = sSurname;
				OurStudent.University = sUniversity;
				OurStudent.StudentCard = int.Parse(sStudentCard);
				OurStudent.Course = int.Parse(sCourse);
				OurStudent.HasStipend = keyHasStipend.Key == ConsoleKey.Y ? true : false;
				OurStudent.HasBudget = keyHasBudget.Key == ConsoleKey.Y ? true : false;


				arrStudents[i] = OurStudent;
			}
			foreach (Student t in arrStudents)
			{

				Console.WriteLine("------------------------------------------------");

				Console.WriteLine("Данi про об`ект: ");
				Console.WriteLine("------------------------------------------------");

				Console.WriteLine("iм'я: " + t.Name);
				Console.WriteLine("Прiзвище: " + t.Surname);
				Console.WriteLine("Унiверситет: " + t.University);
				Console.WriteLine("Витрати на проїзд в день: " + t.StudentCard);
				Console.WriteLine("Кiлькiсть навчальних днiв: " + t.Course);
				Console.WriteLine(t.HasStipend ? "У студента є стiпендiя" : "У студента нема стiпендiї");
				Console.WriteLine(t.HasBudget ? "Студент навчається на бюджетi" : "Студент навчається на контрактi");
				Console.WriteLine();
				Console.WriteLine("Тижневi витрати на проїзд: " + t.PayMoney.ToString("0.00"));
				Console.ReadKey();
			}
		}
	}
}