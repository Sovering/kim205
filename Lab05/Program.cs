﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
	class Student
	{
		public string Name;
		public string Surname;
		public string University;
		public int StudentCard;
		public int Course;
		public bool HasStipend;
		public bool HasBudget;
		public double PayMoney() {
			return StudentCard * Course;
		}
		static void Main(string[] args)
		{
			Console.Write("Введiть iм'я: ");
			string sName = Console.ReadLine();

			Console.Write("Введіть прiзвище:");
			string sSurname = Console.ReadLine();

			Console.Write("Введiть назву унiверситету:");
			string sUniversity = Console.ReadLine();

			Console.Write("Введiть витрати на проїзд за день :");
			string sStudentCard = Console.ReadLine();

			Console.Write("Введiть кількість навчальних днів:");
			string sCourse = Console.ReadLine();

			Console.Write("Чи є у вас стiпендiя? (y-так, n-нi): ");
			ConsoleKeyInfo keyHasStipend = Console.ReadKey();

			Console.WriteLine(); Console.Write("Чи навчаєтесь ви на бюджетi? (y-так, n-нi): ");
			ConsoleKeyInfo keyHasBudget = Console.ReadKey();
			Console.WriteLine();

			Student OurStudent = new Student();

			OurStudent.Name = sName;
			OurStudent.Surname = sSurname;
			OurStudent.University = sUniversity;
			OurStudent.StudentCard = int.Parse(sStudentCard);
			OurStudent.Course = int.Parse(sCourse);
			OurStudent.HasStipend = keyHasStipend.Key == ConsoleKey.Y ? true : false;
			OurStudent.HasBudget = keyHasBudget.Key == ConsoleKey.Y ? true : false;

			double PayMoney = OurStudent.PayMoney();
			Console.WriteLine();
			Console.WriteLine("------------------------------------------------");

			Console.WriteLine("Данi про об`ект: ");
			Console.WriteLine("------------------------------------------------");

			Console.WriteLine("iм'я: " + OurStudent.Name);
			Console.WriteLine("Прiзвище: " + OurStudent.Surname);
			Console.WriteLine("Унiверситет: " + OurStudent.University);
			Console.WriteLine("Витрати на проїзд в день: " + OurStudent.StudentCard);
			Console.WriteLine("Кількість навчальних днів: " + OurStudent.Course);
			Console.WriteLine(OurStudent.HasStipend ? "У студента є стiпендія" : "У студента нема стiпендiї");
			Console.WriteLine(OurStudent.HasBudget ? "Студент навчається на бюджетi" : "Студент навчається на контрактi");
			Console.WriteLine();
			Console.WriteLine("Витрати на проїзд: " +
PayMoney.ToString("0.00"));
			Console.ReadKey();
		}
	}
}
