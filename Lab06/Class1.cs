﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary2
{

	public class Student
	{
		public string Name;
		public string Surname;
		public string University;
		public int StudentCard;
		public int Course;
		public bool HasStipend;
		public bool HasBudget;
		public double PayMoney
		{
			get
			{
				return
			   GetPayMoney();
			}
		}
		public double GetPayMoney()
		{
			return StudentCard * Course;
		}
	}
}

