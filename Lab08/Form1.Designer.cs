﻿namespace lr8
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.tName = new System.Windows.Forms.TextBox();
			this.tCountry = new System.Windows.Forms.TextBox();
			this.tFamily = new System.Windows.Forms.TextBox();
			this.tYears = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.tPaws = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.tPopulation = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.checkPredator = new System.Windows.Forms.CheckBox();
			this.checkWings = new System.Windows.Forms.CheckBox();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.tName);
			this.groupBox1.Controls.Add(this.tCountry);
			this.groupBox1.Controls.Add(this.tFamily);
			this.groupBox1.Controls.Add(this.tYears);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.tPaws);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.tPopulation);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(9, 10);
			this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.groupBox1.Size = new System.Drawing.Size(179, 199);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Student";
			// 
			// tName
			// 
			this.tName.Location = new System.Drawing.Point(93, 28);
			this.tName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.tName.Name = "tName";
			this.tName.Size = new System.Drawing.Size(76, 20);
			this.tName.TabIndex = 4;
			// 
			// tCountry
			// 
			this.tCountry.Location = new System.Drawing.Point(93, 53);
			this.tCountry.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.tCountry.Name = "tCountry";
			this.tCountry.Size = new System.Drawing.Size(76, 20);
			this.tCountry.TabIndex = 5;
			// 
			// tFamily
			// 
			this.tFamily.Location = new System.Drawing.Point(93, 81);
			this.tFamily.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.tFamily.Name = "tFamily";
			this.tFamily.Size = new System.Drawing.Size(76, 20);
			this.tFamily.TabIndex = 6;
			// 
			// tYears
			// 
			this.tYears.Location = new System.Drawing.Point(93, 170);
			this.tYears.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.tYears.Name = "tYears";
			this.tYears.Size = new System.Drawing.Size(76, 20);
			this.tYears.TabIndex = 9;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(20, 170);
			this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(26, 13);
			this.label6.TabIndex = 5;
			this.label6.Text = "Age";
			// 
			// tPaws
			// 
			this.tPaws.Location = new System.Drawing.Point(93, 139);
			this.tPaws.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.tPaws.Name = "tPaws";
			this.tPaws.Size = new System.Drawing.Size(76, 20);
			this.tPaws.TabIndex = 8;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(20, 139);
			this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(40, 13);
			this.label5.TabIndex = 4;
			this.label5.Text = "Course";
			// 
			// tPopulation
			// 
			this.tPopulation.Location = new System.Drawing.Point(93, 108);
			this.tPopulation.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.tPopulation.Name = "tPopulation";
			this.tPopulation.Size = new System.Drawing.Size(76, 20);
			this.tPopulation.TabIndex = 7;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(20, 111);
			this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(69, 13);
			this.label4.TabIndex = 3;
			this.label4.Text = "Student Card";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(20, 84);
			this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(53, 13);
			this.label3.TabIndex = 2;
			this.label3.Text = "University";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(20, 56);
			this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(49, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "Surname";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(20, 28);
			this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(35, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Name";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.checkPredator);
			this.groupBox2.Controls.Add(this.checkWings);
			this.groupBox2.Location = new System.Drawing.Point(9, 214);
			this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.groupBox2.Size = new System.Drawing.Size(118, 93);
			this.groupBox2.TabIndex = 0;
			this.groupBox2.TabStop = false;
			// 
			// checkPredator
			// 
			this.checkPredator.AutoSize = true;
			this.checkPredator.Location = new System.Drawing.Point(22, 58);
			this.checkPredator.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.checkPredator.Name = "checkPredator";
			this.checkPredator.Size = new System.Drawing.Size(60, 17);
			this.checkPredator.TabIndex = 3;
			this.checkPredator.Text = "Budget";
			this.checkPredator.UseVisualStyleBackColor = true;
			// 
			// checkWings
			// 
			this.checkWings.AutoSize = true;
			this.checkWings.Location = new System.Drawing.Point(22, 23);
			this.checkWings.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.checkWings.Name = "checkWings";
			this.checkWings.Size = new System.Drawing.Size(62, 17);
			this.checkWings.TabIndex = 2;
			this.checkWings.Text = "Stipend";
			this.checkWings.UseVisualStyleBackColor = true;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(132, 235);
			this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(56, 19);
			this.button1.TabIndex = 0;
			this.button1.Text = "OK";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(132, 262);
			this.button2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(56, 19);
			this.button2.TabIndex = 1;
			this.button2.Text = "Cancel";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(201, 315);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.groupBox1);
			this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.Name = "Form1";
			this.Text = "Student";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tName;
        private System.Windows.Forms.TextBox tCountry;
        private System.Windows.Forms.TextBox tFamily;
        private System.Windows.Forms.TextBox tYears;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tPaws;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tPopulation;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkPredator;
        private System.Windows.Forms.CheckBox checkWings;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}

